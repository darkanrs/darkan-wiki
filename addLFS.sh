#!/bin/bash

# Set the size threshold in megabytes
SIZE_THRESHOLD_MB=50

# Find files above the size threshold (including subdirectories)
FILES=$(find . -type f -size +${SIZE_THRESHOLD_MB}M -not -path "./.git/*")

# Initialize Git LFS if not already initialized
if [ ! -e .gitattributes ]; then
    git lfs install
fi

# Iterate over the found files and track them with Git LFS
for FILE in $FILES; do
    echo "$FILE"
    git lfs track "$FILE"
done

# Commit the changes to the .gitattributes file
git add .gitattributes
git commit -m "Enable Git LFS for large files"

echo "Git LFS setup complete. You can now push your changes to the remote repository."

